def call(Map pipelineParams) {

def app

pipeline {
    agent {
        docker { image 'docker' }
    }
    stages {
        stage('build') {
            steps {
                script {
                    app = docker.build("hip911/http-counter","-f app/Dockerfile --build-arg PYTHON_VERSION=${pipelineParams.python_version} app/")
                }
            }
        }

        stage('Push image') {
            steps {
                script {
                    /* Finally, we'll push the image with two tags:
                     * First, the incremental build number from Jenkins
                     * Second, the 'latest' tag.
                     * Pushing multiple tags is cheap, as all the layers are reused. */
                    docker.withRegistry('https://registry.hub.docker.com', 'docker-hub-credentials') {
                        app.push("${env.BUILD_NUMBER}")
                        app.push("latest")
                    }
                }
            }
        }
    }
}
}
